package br.edu.cest.dev;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       Livro liv01 = new Livro();
       liv01.setAutor("spooky naz");
       liv01.setEdicao("1");
       liv01.setVolume("1");
       
       Item item01 = new Item();
       item01.setAnoPublicacao("2017");
       item01.setEditora("chrollo");
       item01.setIsbn("9780374530716");
       item01.setPreco("10");
       item01.setTitulo("O mundo sombrio de sabrina");
       
       liv01.display();
       item01.display();
       
       ParImpar valor = new ParImpar();
       valor.retorno();
    	   
       
    }
}
