package br.edu.cest.dev;

public class Livro extends Item {
	private String autor;
	private String edicao;
	private String volume;
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getEdicao() {
		return edicao;
	}
	public void setEdicao(String edicao) {
		this.edicao = edicao;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	
	@Override 
	public void display () {
		System.out.println("Autor: " + this.getAutor());
		System.out.println("Edição: " + this.getEdicao());
		System.out.println("Volume: " + this.getVolume());
		
		
	
	}
	

}
