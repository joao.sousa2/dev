package br.edu.cest.dev;

public class Item {
	private String titulo;
	private String editora;
	private String anoPublicacao;
	private String isbn;
	private String preco;
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getEditora() {
		return editora;
	}
	public void setEditora(String editora) {
		this.editora = editora;
	}
	public String getAnoPublicacao() {
		return anoPublicacao;
	}
	public void setAnoPublicacao(String anoPublicacao) {
		this.anoPublicacao = anoPublicacao;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getPreco() {
		return preco;
	}
	public void setPreco(String preco) {
		this.preco = preco;
	}
	
	public void display() {
		System.out.println("Titulo: " + this.getTitulo()); 
		System.out.println("Editora: " + this.getEditora());
		System.out.println("Ano de publicação: " + this.getAnoPublicacao());
		System.out.println("ISBN: " + this.getIsbn());
		System.out.println("Preço: " + this.getPreco());
	}





}
